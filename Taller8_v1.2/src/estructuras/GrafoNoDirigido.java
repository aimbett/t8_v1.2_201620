package estructuras;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * Clase que representa un grafo no dirigido con pesos en los arcos.
 * @param K tipo del identificador de los vertices (Comparable)
 * @param E tipo de la informacion asociada a los arcos

 */
public class GrafoNoDirigido<K extends Comparable<K>, E> implements Grafo<K,E> {

	/**
	 * Nodos del grafo
	 */
	//TODO Declare la estructura que va a contener los nodos
	private Lista<NodoGrafo> nodos;
	/**
	 * Lista de adyacencia 
	 */
	private HashMap<String, List<Arco<K,E>>> adj;
	//TODO Utilice sus propias estructuras (defina una representacion para el grafo)
	// Es libre de implementarlo con la representacion de su agrado. 

	private boolean[][] grafo;

	private Lista<Arco<K,E>> arcos;

	/**
	 * Construye un grafo no dirigido vacio.
	 */
	public GrafoNoDirigido() {
		//TODO implementar
		grafo = new boolean[0][0];
		nodos = new Lista<>();
		arcos = new Lista<>();
		adj = new HashMap<>();
	}

	@Override
	public boolean agregarNodo(Nodo<K> nodo) {
		//TODO implementar
		Iterator t = nodos.iterator();

		while(t.hasNext())
		{
			NodoGrafo actual = (NodoGrafo) t.next();
			if (actual.darNodo().compareTo(nodo)==0)
			{
				return false;
			}
		}

		
		nodos.encolar(new NodoGrafo<Nodo,K>(nodo.darId(), nodo));
		return true;

	}

	@Override
	public boolean eliminarNodo(K id) {
		//TODO implementar
		if(buscarNodo(id)==null)
		{
			return false;
		}
		Iterator<NodoGrafo> t = nodos.iterator();
		int pos =0;
		while(t.hasNext())
		{
			NodoGrafo actual = t.next();
			pos++;
			if(actual.darId()==id)
			{
				break;
			}
		}

		nodos.remove(pos);
		return true;
	}

	@Override
	public Arco<K,E>[] darArcos() {
		//TODO implementar
		Arco[] resp = new Arco[arcos.darTamanio()];
		Iterator t = arcos.iterator();
		int cant = 0;
		while(t.hasNext())
		{
			Arco actual = (Arco) t.next();
			resp[cant] = actual;
			cant ++;
		}
		return resp;
	}

	private Arco<K,E> crearArco( K inicio, K fin, double costo, E e )
	{
		Nodo<K> nodoI = buscarNodo(inicio);
		Nodo<K> nodoF = buscarNodo(fin);
		if ( nodoI != null && nodoF != null)
		{
			return new Arco<K,E>( nodoI, nodoF, costo, e);
		}
		{
			return null;
		}
	}

	@Override
	public Nodo<K>[] darNodos() {
		//TODO implementar
		Nodo<K>[] resp = new Nodo[nodos.darTamanio()];
		Iterator t = nodos.iterator();
		int cant = 0;
		while(t.hasNext())
		{
			Nodo actual = (Nodo) t.next();
			resp[cant] = actual;
			cant ++;
		}
		return resp;
	}

	@Override
	public boolean agregarArco(K inicio, K fin, double costo, E obj) {
		//TODO implementar
		if(buscarNodo(inicio)== null || buscarNodo(fin) == null )
		{
			return false;
		}

		Arco arc = crearArco(inicio, fin, costo, obj);
		arcos.encolar(arc);
		return true;
	}

	@Override
	public boolean agregarArco(K inicio, K fin, double costo) {
		return agregarArco(inicio, fin, costo, null);
	}

	@Override
	public Arco<K,E> eliminarArco(K inicio, K fin) {
		//TODO implementar
		Iterator t = arcos.iterator();
		int pos = 0;
		while(t.hasNext())
		{
			pos ++;
			Arco actual = (Arco) t.next();
			if(actual.darNodoInicio().darId() == inicio && actual.darNodoFin().darId()== fin)
			{
				return arcos.remove(pos);
			}
		}

		return null;
	}

	@Override
	public NodoGrafo buscarNodo(K id) {
		//TODO implementar
		Iterator t = nodos.iterator();

		while(t.hasNext())
		{
			NodoGrafo actual = (NodoGrafo) t.next();
			if(actual.darId().compareTo(id)==0)
			{
				return actual;
			}
		}
		return null;
	}

	@Override
	public Arco<K,E>[] darArcosOrigen(K id) {
		//TODO implementar
		Iterator<Arco<K,E>> t = arcos.iterator();
		Lista<Arco<K,E> > temp = new Lista<>();
		while(t.hasNext())
		{
			Arco actual = t.next();
			if(actual.darNodoInicio().darId()==id)
			{
				temp.encolar(actual);
			}
			
		}
		
		Arco[] resp = new Arco[temp.darTamanio()];
		Iterator t2 = temp.iterator();
		int cant = 0;
		
		while(t2.hasNext())
		{
			resp[cant] = (Arco) t2.next();
			cant++;
		}
		
		return resp;
	}

	@Override
	public Arco<K,E>[] darArcosDestino(K id) {
		//TODO implementar
		Iterator<Arco<K,E>> t = arcos.iterator();
		Lista<Arco<K,E> > temp = new Lista<>();
		while(t.hasNext())
		{
			Arco actual = t.next();
			if(actual.darNodoFin().darId()==id)
			{
				temp.encolar(actual);
			}
			
		}
		
		Arco[] resp = new Arco[temp.darTamanio()];
		Iterator t2 = temp.iterator();
		int cant = 0;
		
		while(t2.hasNext())
		{
			resp[cant] = (Arco) t2.next();
			cant++;
		}
		
		return resp;
	}

}
