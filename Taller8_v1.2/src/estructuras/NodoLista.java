package estructuras;

public class NodoLista<T> {
	private NodoLista<T> siguiente;

	private T valor;

	public NodoLista(T pValor){
		valor = pValor;
		siguiente = null;
	}

	public NodoLista<T> getSiguiente() {
		return siguiente;
	}

	public void setSiguiente(NodoLista<T> pSiguiente) {
		this.siguiente = pSiguiente;
	}

	public T getValor() {
		return valor;
	}

	public void setValor(T pValor) {
		this.valor = pValor;
	}
}
