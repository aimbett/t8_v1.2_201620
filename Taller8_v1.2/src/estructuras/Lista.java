package estructuras;

import java.util.Iterator;




public class Lista<T> {

	private int tamanio ;

	private NodoLista<T> primero;
	private NodoLista<T> ultimo;

	public Lista(){
		tamanio = 0;
		primero = null;
		ultimo = null;
	}


	public T pop(){
		T temp =primero.getValor();
		primero = primero.getSiguiente();
		tamanio--;
		return temp;
	}
	
	public int size()
	{
		return tamanio;
	}
	public NodoLista<T> darPrimero(){
		return primero;
	}

	public void agregarPush( T objeto){

		NodoLista<T> temp = primero;
		primero = new NodoLista<T>(objeto);
		primero.setSiguiente(temp);
		tamanio++;

	}
	public int darTamanio(){
		return tamanio;
	}

	//Cuando pertenezca a una cola.
	public void encolar(T objeto){
		NodoLista<T> nuevo = new NodoLista<T>(objeto);
		if( primero == null){

			primero = nuevo;

			ultimo = nuevo;
		}
		else{
			ultimo.setSiguiente(nuevo);
			ultimo = nuevo;
		}
		tamanio ++;
	}

	public T desencolar(){
		T retorno = primero.getValor();
		primero = primero.getSiguiente();
		tamanio--;
		return retorno;

	}
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new IteratorFila();
	}

	public String toString(){
		String resp ="";
		Iterator t = iterator();
		while(t.hasNext())
		{
			T act = (T) t.next();
			resp += "\n \n"+act.toString();
		}

		return resp;
	}

	
	public boolean isEmpty() {
		return tamanio==0;
	}
	public T get(int index) {

		if(index>tamanio-1||index<0)throw new IndexOutOfBoundsException();

		NodoLista<T> actual = primero;

		while(index--!=0){
			actual = actual.getSiguiente();
		}

		return actual.getValor();
	}

	public T set(int index, T element) {

		NodoLista<T> actual = primero;

		while(index-->0){
			actual = actual.getSiguiente();
		}
		actual.setValor(element);

		return null;
	}

	public T remove(int index) {

		if(index<0||index>tamanio-1)throw new IndexOutOfBoundsException();

		T elem = null;

		if(index == 0){
			elem = (T) primero.getValor();
			primero = primero.getSiguiente();
			ultimo = --tamanio==0? null:ultimo;
		}else{
			NodoLista<T> actual = primero;
			while(index--!=1){
				actual = actual.getSiguiente();
			}
			elem = (T)actual.getSiguiente().getValor();
			actual.setSiguiente(actual.getSiguiente().getSiguiente());
			tamanio--;
		}

		return null;
	}
	private class IteratorFila implements Iterator<T>{

		private NodoLista<T> actual = primero;

		public boolean hasNext() {
			// TODO Auto-generated method stub
			return actual!=null;
		}

		public T next() {
			// TODO Auto-generated method stub
			T ret = actual.getValor();
			actual = actual.getSiguiente();
			return ret;
		}

		public void remove() {
			// TODO Auto-generated method stub

		}
	}
}
