package mundo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

import modelos.Estacion;
import modelos.Ruta;
import estructuras.Arco;
import estructuras.Grafo;
import estructuras.GrafoNoDirigido;
import estructuras.Lista;
import estructuras.Nodo;
import estructuras.NodoGrafo;

/**
 * Clase que representa el administrador del sistema de metro de New York
 *
 */
public class Administrador {

	/**
	 * Ruta del archivo de estaciones
	 */
	public static final String RUTA_PARADEROS ="./data/stations.txt";

	/**
	 * Ruta del archivo de rutas
	 */
	public static final String RUTA_RUTAS ="./data/routes.txt";


	/**
	 * Grafo que modela el sistema de metro de New York
	 */
	//TODO Declare el atributo grafo No dirigido que va a modelar el sistema. 
	// Los identificadores de las estaciones son String
	private GrafoNoDirigido<String,Ruta> grafo;


	private Lista<Ruta> rutas;

	/**
	 * Construye un nuevo administrador del Sistema
	 */
	public Administrador() 
	{
		//TODO inicialice el grafo como un GrafoNoDirigido
		grafo = new GrafoNoDirigido<String,Ruta>();
		rutas = new Lista<>();

	}

	/**
	 * Devuelve todas las rutas que pasan por la estacion con nombre dado
	 * @param identificador 
	 * @return Arreglo con los identificadores de las rutas que pasan por la estacion requerida
	 */
	public String[] darRutasEstacion(String identificador)
	{
		//TODO Implementar
		Lista<String> lista = new Lista<>();
		Iterator it = rutas.iterator();
		while(it.hasNext())
		{
			Ruta actual = (Ruta) it.next();
			if(actual.existeEstacion(identificador))
			{
				lista.encolar(actual.getNombre());
			}
		}
		
		String[] resp = new String[lista.darTamanio()];
		
		Iterator t2 = lista.iterator();
		int pos =0;
		while(t2.hasNext())
		{
			resp[pos] = (String) t2.next();
			pos++;
		}
		
		return resp;
	}

	/**
	 * Devuelve la distancia que hay entre las estaciones mas cercanas del sistema.
	 * @return distancia minima entre 2 estaciones
	 */
	public double distanciaMinimaEstaciones()
	{
		//TODO Implementar
		double min = Double.MAX_VALUE;
		Iterator it = rutas.iterator();
		while(it.hasNext())
		{
			Ruta actual = (Ruta) it.next();
			if(actual.distMinima()<min)
			{
				min = actual.distMinima();
			}
		}
		
		return min;
	}

	/**
	 * Devuelve la distancia que hay entre las estaciones más lejanas del sistema.
	 * @return distancia maxima entre 2 paraderos
	 */
	public double distanciaMaximaEstaciones()
	{
		//TODO Implementar
		return 0.0;
	}


	/**
	 * Metodo encargado de extraer la informacion de los archivos y llenar el grafo 
	 * @throws Exception si ocurren problemas con la lectura de los archivos o si los archivos no cumplen con el formato especificado
	 */
	public void cargarInformacion() throws Exception
	{
		//TODO Implementar
		BufferedReader bReader = new BufferedReader(new FileReader(new File(RUTA_PARADEROS)));


		String linea = bReader.readLine();
		linea = bReader.readLine();
		linea = linea.trim();
		int cont = 0;
		while (linea!= null)
		{
			String[] datos = linea.split(";");
			String nombre = datos[0];
			double lat = Double.parseDouble(datos[1]);
			double lon = Double.parseDouble(datos[2]);

			Estacion<String> est = new Estacion<String>(nombre, lat, lon);
			grafo.agregarNodo(est);
			linea = bReader.readLine();
			cont++;
		}

		bReader.close();
		
		System.out.println("Se han cargado correctamente "+grafo.darNodos().length+" Estaciones");





		//TODO Implementar
		BufferedReader bReader2 = new BufferedReader(new FileReader(new File(RUTA_RUTAS)));

		String linea2 = bReader2.readLine();
		int numeroRutas = Integer.parseInt(linea2);
		for (int i = 1 ; i<= numeroRutas;i++)
		{
			
			linea2 = bReader2.readLine();
			String nombre = bReader2.readLine();
			int cantEstaciones = Integer.parseInt(bReader2.readLine());
			
			Lista<Estacion> lista = new Lista<>();
			Lista<Double> costos = new Lista<>();

			for(int j =1 ;j<=cantEstaciones;j++)
			{

				String linActual = bReader2.readLine();
				String[] datosActual = linActual.split(" ");
				String est = datosActual[0];
				
				double dist = Double.parseDouble(datosActual[1]);
				Estacion<String> esta = buscarEstacion(est);
				if(esta != null)
				{
					
				lista.encolar(esta);
				}
				else
				{
					
				}
				
				costos.encolar(dist);
			}

			
			Ruta rutaActual = new Ruta(nombre, lista,costos);

			rutas.encolar(rutaActual);

		}

		bReader2.close();
		
		
		

		Iterator<Ruta> it = rutas.iterator();
		while(it.hasNext())
		{
			Ruta actual = it.next();
			System.out.println(actual.getNombre());
			System.out.println("Num Estaciones: "+actual.getEstaciones().darTamanio());
			
			
			Iterator<Estacion> itEst = actual.getEstaciones().iterator(); 
			
			Iterator<Double> itCost = actual.getCostos().iterator(); 
			Estacion anterior = null;
			while(itEst.hasNext() && itCost.hasNext())
			{
				
				Estacion estActual = itEst.next();
				
				double costo = itCost.next();
				if(estActual!= null)
				{
					
					
					if(costo >0 && anterior!=null)
					{
						
						grafo.agregarArco(anterior.darId()+"", estActual.darId()+"", costo, actual);
					}


					anterior = estActual;
				}

			}
		}

		
		

		System.out.println("Se han cargado correctamente "+ grafo.darArcos().length+" arcos");
	}

	/**
	 * Busca la estacion con identificador dado<br>
	 * @param identificador de la estacion buscada
	 * @return estacion cuyo identificador coincide con el parametro, null de lo contrario
	 */
	public Estacion buscarEstacion(String identificador)
	{
		//TODO Implementar
		NodoGrafo nodo =  grafo.buscarNodo(identificador);
		return (Estacion) nodo.darNodo();
	}

}
