package modelos;

import java.util.Iterator;

import estructuras.Lista;

public class Ruta {

	private String nombre;
	private Lista<Estacion> estaciones;
	private Lista<Double> costos;
	
	
	
	
	public Ruta(String nombre, Lista<Estacion> estaciones, Lista<Double> costos) {
		super();
		this.nombre = nombre;
		this.estaciones = estaciones;
		this.costos = costos;
	}
	
	
	
	public Lista<Double> getCostos() {
		return costos;
	}



	public void setCostos(Lista<Double> costos) {
		this.costos = costos;
	}



	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Lista<Estacion> getEstaciones() {
		return estaciones;
	}
	public void setEstaciones(Lista<Estacion> estaciones) {
		this.estaciones = estaciones;
	}
	
	public boolean existeEstacion(String id)
	{
		Iterator it = estaciones.iterator();
		while(it.hasNext())
		{
			Estacion act = (Estacion) it.next();
			if(act.darId().equals(id))
			{
				return true;
			}
			
		}
		return false;
	}
	
	public double distMinima()
	{
		double min = Double.MAX_VALUE;
		Iterator it = costos.iterator();
		while(it.hasNext())
		{
			double actual = (double) it.next();
			if(actual<min && actual >0)
			{
				min = actual;
			}
			
		}
		return min;
	}
	
}
